﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Proyecto._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Aprendiendo Misak</title>
    <!-- Bootstrap core CSS -->

     <!-- ENSAYOS GIT -->
    <link href="public/jquery-ui.css" rel="stylesheet">
    <link href="public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template -->
    <link href="public/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet"
        type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"
        type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet'
        type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic'
        rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700'
        rel='stylesheet' type='text/css'>
    <!-- Custom styles for this template -->
    <link href="public/css/agency.min.css" rel="stylesheet">
    <style type="text/css">
        .img-fluid
        {
            max-width: 100%;
            height: auto;
        }
        .img-juego
        {
            width: 157px;
            height: 157px;
        }
        .style2
        {
            width: 158px;
        }
        .style3
        {
            width: 167px;
        }
        .style4
        {
            width: 186px;
        }
        .style5
        {
            width: 165px;
        }
        .style6
        {
            width: 300px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <body id="page-top">
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Namtrik Kusrekun</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Información</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Cronología</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">Juegos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#team">Equipo</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Diccionario</a>
            </li>
             <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#sugerencias">Sugerencias</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
        <!-- Header/ pagian de Inicio -->
        <header class="masthead" style="padding-top: 10rem;" > 
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in">Namui Wam Wan Kusrekun</div>
        </div>
      </div>
    </header>
        <!-- Services -->
        <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">INFORMACIÓN</h2>
          </div>
        </div>
        <div class="row text-center">
        <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-university fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Cronologia</h4>
            <p class="text-muted"align="justify"> Es la ciencia que estudia y precisa el orden y fechas de los acontecimientos
                 históricos, este proceso o ciencia organiza u ordena de manera sucesiva o gradual cada hecho que acontece en el mundo en un determinado momento y espacio. Esta ciencia es muy importante para la historia ya que la historia es la ciencia que estudia el pasado de la humanidad y como bien se dijo anteriormente ayuda a organizar cada hecho importante acontecido en el mundo gradualmente.
            </p>
          </div>
         <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-gamepad fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Juegos</h4>
           <p class="text-muted"align="justify">Los juegos se realizaron didácticos con el objetivo de que el usuario adquiera conocimiento sobre el lenguaje Misak, el usuario debe finalizar cada nivel para así poder pasar al siguiente.</p>
          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-book fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Diccionario</h4>
            <p class="text-muted"align="justify">Este incluye una amplia serie de palabras y términos ordenados de forma alfabética y cuya orientación es de consulta. La compilación de Dicho es muy común, se refiere a tipos de información sobre cada palabra, como: su significado y la forma en que se escribe.</div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Seguridad Web</h4>
            <p class="text-muted"aling="justify"></div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-hand-paper-o fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Equipo</h4>
            <p class="text-muted"></div>
      </div>
    </section>
        <!-- Portfolio Grid -->
        <section class="bg-light" id="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Historia</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="public/img/portfolio/imagen-misak.jpg" alt ; style=" width: 400px; 
                height: 265px">
            </a>
            <div class="portfolio-caption">
              <h4>Misak</h4>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid"  src="public/img/portfolio/imagen-idioma.jpg "   alt ; style=" width: 400px; 
                height: 265px">
               
                    
            </a>
            <div class="portfolio-caption">
              <h4>Idioma</h4>
             
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="public/img/portfolio/03-thumbnail.jpg" alt="">
            </a> 
            <div class="portfolio-caption">
              <h4>Vestuario</h4>
              
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="public/img/portfolio/imagen-gastronomia.jpg"  alt ; style=" width: 400px; 
                height: 265px">
            </a>
            <div class="portfolio-caption">
              <h4>Gastronomia</h4>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="public/img/portfolio/imagen-mitos.jpg" alt ; style=" width: 400px; 
                height: 265px">
            </a>
            <div class="portfolio-caption">
              <h4>Mitos y Leyendas</h4>
            </div>
          </div>
          
          <div class="col-md-4 col-sm-6 portfolio-item">
            
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
          </div>
        </div>
      </div>
    </section>
        <!-- About -->
        <section id="about">
      <div class="container">
      <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>3 Col Portfolio - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/3-col-portfolio.css" rel="stylesheet">

  </head>

  <body>
    <!-- juegos didacticos-->

      <!-- Page Heading -->
      <h1 class="my-4"><p class="text-rediel"align="center">JUEGOS
        <small></small>
      </h1>

      <div class="row">
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <img class="card-img-top" src="public/img/about/sopaDeLetras.png" alt="">
            <div class="card-body">
              <h4> <a class="nav-link js-scroll-trigger" runat="server" Target="_blank" href="Juegos/sopaDeLetras/sopadeletras2.html  ">Sopa de letras</a> </h4>
              <p class="card-text">Consiste en descubrir un número determinado de palabras enlazando estas letras de forma horizontal, vertical, diagonal y en cualquier sentido, tanto de derecha a izquierda como de izquierda a derecha, y tanto de arriba abajo, como de abajo arriba.</div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <img class="card-img-top" src="public/img/about/asociacion.png" alt="">
            <div class="card-body">
              <h4><asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="~/Juegos/Asociación/imagenrelacion.html" 
                            Target="_blank">asociación de palabras con imágenes</asp:HyperLink></h4>
              <p class="card-text">Este juego permite estimular la capacidad de relacionar palabras e imágenes con contenidos conceptuales y situaciones de la vida cotidiana.</div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <img class="card-img-top" src="public/img/about/Crucigrama.jpg" alt="">
            <div class="card-body">
              <h4><asp:HyperLink ID="HyperLink9" runat="server" Text="Crucigrama" NavigateUrl="Juegos/Crucigrama/palabrasecreta.html"
                             Target="_blank">Palabra Secreta</asp:HyperLink></h4>
              <p class="card-text">Para completar  la palabra secreta, la persona debe leer bien la definición que se presenta, unas correspondientes al sentido vertical y otras al horizontal, si la persona conoce la palabra mostrada puede ingresarla al crucigrama.</div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <img class="card-img-top" src="public/img/about/encuentrelafigura.jpg" alt="">
            <div class="card-body">
              <h4><asp:HyperLink ID="HyperLink10" runat="server" Target="_blank" NavigateUrl="~/Juegos/EncuentreLaImagen/memoria.html" 
                    Text="Encuentre la Figura">Encuentre la Imagen</asp:HyperLink></h4>
              <p class="card-text">Este es un juego didáctico con el objetivo de estimular el cerebro del usuario para que así desarrolle el conocimiento sobre el lenguaje Misak</div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <img class="card-img-top" src="public/img/about/abecedario.jpg" alt="">
            <div class="card-body">
              <h4><asp:HyperLink ID="HyperLink11" runat="server" Text="Crucigrama" NavigateUrl="~/Juegos/Rompecabezas/puzle.html"
                             Target="_blank">Rompecabezas</asp:HyperLink></h4>
              <p class="card-text">Un rompecabezas o puzle es un juego de mesa cuyo objetivo es formar una figura combinando correctamente las partes de esta, que se encuentran en distintos pedazos o piezas planas.</div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <img class="card-img-top" src="public/img/about/Números.jpg" alt="">
            <div class="card-body">
              <h4><asp:HyperLink ID="HyperLink12" runat="server" 
                    Target="_blank" NavigateUrl="~/Juegos/ParejasJuego/juegopareja.html" 
                    Text="Encuentre la Figura">Encuentre la Pareja de Imagenes</asp:HyperLink></h4>
              <p class="card-text">La palabra Número proviene del latín numĕrus, con el mismo significado. Es todo signo o símbolo utilizado para designar cantidades, valores o entidades que se comportan como cantidades.</div>
          </div>
        </div>
      </div>
      <!-- /.row -->

      <!-- Pagination -->
      <ul class="pagination justify-content-center">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">1</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">2</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">3</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>

    </div>
    <!-- /.container -->
    </section>
        <!-- Team -->

        <!-- PRIMEROS ENSAYOS DE GIT -->
        <section class="bg-light" id="team">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Equipo de trabajo</h2>
            <h3 class="section-subheading text-muted">Universidad Cooperativa de Colombia   </h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="public/img/team/Brayan.png" alt="">
              <h4>Brayan Alexi Morales</h4>
              <p class="text-muted">Estudiante</p>
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="https://www.google.com/intl/es-419/gmail/about/">
                    <i class="fa fa-envelope-o"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a href="https://outlook.office.com/owa/?realm=campusucc.edu.co&exsvurl=1&ll-cc=3082&modurl=0">
                    <i class="fa fa-at"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="public/img/team/Miguel-Dario.jpg" alt="">
              <h4>Miguel Dario Tunubala</h4>
              <p class="text-muted">Estudiante</p>
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="https://www.google.com/intl/es-419/gmail/about/">
                    <i class="fa fa-envelope-o"></i>
                  </a>
                </li>
                </a>
                </li>
                <li class="list-inline-item">
                  <a href="https://outlook.office.com/owa/?realm=campusucc.edu.co&exsvurl=1&ll-cc=3082&modurl=0">
                    <i class="fa fa-at"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="public/img/team/mambuscay.jpeg" alt="">
              <h4>Estiven Mambuscay</h4>
              <p class="text-muted">Estudiante</p>
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="https://www.google.com/intl/es-419/gmail/about/">
                    <i class="fa fa-envelope-o"></i>
                  </a>
                </li>
               <li class="list-inline-item">
                  <a href="https://outlook.office.com/owa/?realm=campusucc.edu.co&exsvurl=1&ll-cc=3082&modurl=0">
                    <i class="fa fa-at"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>           
        </div>
      </div>
    </section>
        <!-- Clients -->
        <section class="py-5">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
              <a href="#">
              &nbsp;</a></div>
          <div class="col-md-3 col-sm-6">
              <a href="#">
              &nbsp;</a></div>
          <div class="col-md-3 col-sm-6">
              <a href="#">
              &nbsp;</a></div>
          <div class="col-md-3 col-sm-6">
              <a href="#">
              &nbsp;</a></div>
        </div>
      </div>
    </section>
        <!-- Contact -->
        <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">DICCIONARIO</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
          
          
          <asp:TextBox runat="server" ID="txtPalabra"></asp:TextBox>
            <%--<form id="contactForm" name="sentMessage" novalidate>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" id="name" type="text" placeholder="Your Name *" required data-validation-required-message="Please enter your name.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="email" type="email" placeholder="Your Email *" required data-validation-required-message="Please enter your email address.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required data-validation-required-message="Please enter your phone number.">
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" id="message" placeholder="Your Message *" required data-validation-required-message="Please enter a message."></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" class="btn btn-xl" type="submit">Send Message</button>
                </div>
              </div>
            </form>--%>
              <br />
              <br />
              <br />
              <table class="table table-striped" style="background-color: White">
                 <thead>
                    <tr>
                      <th>Español</th>
                      <th>Misak</th>
                      <th>Ingles</th>
                      <th>descripción</th>
                      <th>imagen</th>
                    </tr>
                  </thead>
                  <tbody>
                        <tr>
                            <td class="style2">
                                <asp:Label ID="lbespañol" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="style3">
                                <asp:Label ID="lbmisak" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="style4">
                                <asp:Label ID="lbingles" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="style5">
                                <asp:Label ID="lbdescripcion" runat="server" Text=""></asp:Label></td>
                            <td class="style6">
                                <asp:Image ID="Imagen" runat="server"  Height="100px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="style2">
                                &nbsp;</td>
                            <td class="style3">
                                &nbsp;</td>
                            <td class="style4">
                                &nbsp;</td>
                            <td class="style5">
                                &nbsp;</td>
                            <td class="style6">
                                &nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
          </div>
        </div>
      </div>
    </section>
    <!-- sugerencias -->
        <section id="sugerencias">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Sugerencias</h2>
            <h3 class="section-subheading" >Contribuye a la comunidad nam-trik.</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <form id="contactForm" name="sentMessage" novalidate>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <asp:TextBox class="form-control" id="nombre" type="text" placeholder="nombre completo   *" runat="server" required data-validation-required-message="Ingrese su nombre por favor."></asp:TextBox>
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <asp:TextBox class="form-control" id="correo" type="email" placeholder="correo *" runat="server" required data-validation-required-message="Ingrese su correo por favor"></asp:TextBox>
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                     <asp:TextBox class="form-control" id="telefono" type="tel" placeholder="numero de celular*" runat="server" required data-validation-required-message="Ingrese el telefono por favor."></asp:TextBox>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <asp:TextBox class="form-control" id="Comentario" placeholder="Escribir tu comentario *" runat="server" TextMode="MultiLine" required data-validation-required-message="Ingrese un mensaje por favor."></asp:TextBox>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <asp:Button id="btnSugerencias" class="btn btn-xl"  Text="Enviar" runat="server" OnClick="btnSugerencias_Click"></asp:Button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
        <!-- Footer -->
        <!-- Portfolio Modals -->
        <!-- Modal 1 -->
        <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-dismiss="modal">
                        <div class="lr">
                            <div class="rl">
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 mx-auto">
                                <div class="modal-body">
                                    <!-- Project Details Go Here -->
                                    <h2>
                                        Cultura Misak</h2>
                                    <img class="img-fluid d-block mx-auto"src="public/img/portfolio/imagen-misak.jpg"alt="">
                                    <p class="text-muted"align="justify" style="font-family: sans-serif">                                    
                                        Los Misak somos nacidos de aquí, de la naturaleza como nace un árbol, somos de aquí
                                        desde siglos, de esta raíz. nuestros mayores lo saben hoy como lo han sabido siempre;
                                        saben que no somos traídos, por eso hablan así: Primero era la tierra… y eran las
                                        lagunas… grandes lagunas. La mayor de todas era la de piendamo, en el centro de
                                        la sabana, del páramo, como una matriz, como un corazón. El agua es vida. Primero
                                        eran la tierra y el agua. El agua no es buena ni mala. De allá resultan cosas buenas
                                        y cosas malas. Allá, en las alturas, era el agua. Llovía inmensamente, con aguacero
                                        borrascas, tempestades. Los ríos venían grandes, con inmensos derrumbes que arrastraban
                                        las montañas y traían piedras como casas. Venían grandes crecientes e inundaciones.
                                        Era el agua mala. En ese tiempo, estas profundas aguaicadas y estas peñas no eran
                                        así, como las vemos hoy, esos ríos las hicieron cuando corrieron hasta formar el
                                        mar. El agua es vida. Nace en las cabeceras y baja en los ríos hasta el mar. Y se
                                        devuelve, pero no por los mismos ríos sino por el aire, por la nube. Subiendo por
                                        las aguaicadas y por los filos de las montañas alcanza hasta el páramo, hasta las
                                        sabanas, y cae otra vez la lluvia, cae el agua que es buena.1 Los Misak, como todos
                                        los pueblos, tenemos además de nuestros pasados históricos un origen cultural, lejano
                                        y legendario, como otros pueblos del mundo, igual que las restantes civilizaciones
                                        que conservan sus creencias, en las cuales tienen una visión de mundo propio de
                                        acuerdo a las cosmovisiones y ciclos de vida.
                                        Así mismo, los Misak tenemos nuestras tradición de origen, que la recibimos de nuestros
                                        mayores y debemos trasmitirla a nuestros hijos.

                                    </p>

                                     <p>

                                       <!-- “primero fue las tierras y junto con ella estaba el agua. En las
                                       cabeceras de las sabanas había grandes lagunas. La principal de ella era Nupi o
                                        Piendamo. También había ciénagas y barriales que recogían muchas aguas y se unían
                                        con las lagunas. Las aguas nacían de los Pikap, ojos de agua que quedaban en el
                                        centro, y todos se iban reuniendo para formar un rio grande que corría hacia abajo.
                                        “En la laguna que era una gran saliva, estaba tata illimbi y mama Kelshi su esposa.
                                        Allí estaba. De todas estas cabeceras y de ellos venían el rio grande; de allí se
                                        desprendía las aguas y se iban regando. De allí salían todas las aguas para llegar
                                        al mar. En esa primera época las aguas no subían (en forma de nubes) desde el mar,
                                        solamente bajaban”2 “Cuando llegaron hasta el mar y se recogieron en él, se levantó
                                        la nube y comenzó a subir por las montañas y las cañadas (Ip arrup). Desde ntonces
                                        todas las aguas fueron y se recogieron en el mar y luego regresaron, en nube(mayampi).y
                                        desde entonces todas las aguas es buena y es mala: “maya pi ip atup tapik kak”.
                                        De ella nacen muchas cosas buenas de la tierra el agua hace inundaciones, dicen
                                        los blancos, porque solo ven lo que tiene de malo. Pero los Misak vemos que las
                                        aguas hacen mal y hacen bien. Es un mal porque arrastra tierras, piedras, árboles,
                                        puentes, animales, sembrados, cosas, gente. Pero para nuestros mayores fueron también
                                        un bien porque cuando se formaba un derrumbe, bajaban los shau, residuos, y de ellos
                                        salía un niño o una niña llorando (piunө y pishau) que se sacaban, se lo criaban”
                                        “ellos serían los jefes que tienen historias grandes. De ellos nacen todos los valores
                                        culturales. En esos derrumbes llegaron los Caciques como piendamú, Calambás, José
                                        Ignacio Tumpe, Teresita de la Estrella, Mamá Manuela Karamaya. De ellos venimos
                                        nosotros todos los Misak.” De ahí que para nosotros las inundaciones y derrumbes
                                        no son solo eso, sino que tienen historias. También sabemos de niños y de niñas
                                        del agua que vienen en el rio cada 35 o 60 años y van a ser cultivadores, que van
                                        a enseñar para ser sembradores de semillas. Otros vienen cada 100 años y vienen
                                        con los colores brillosos, son los que cultivan oro y son los más importantes. “El
                                        término Wambia es mencionado por primera vez por el cronista Cieza de León en la
                                        visita a la Nueva Granada al llegar a Popayán. Para nuestros tiempos se ha ido reconstruyendo
                                        la memoria a partir del fortalecimiento de nuestro idioma y se ha encontrado la
                                        palabra apropiada para nominar al Wampiano en el NAMUI WAM (nuestra voz) y es Misak-Misak
                                        PI Urek, cuyo significado es, Gente Hijos del Agua. El origen de los Misak desde
                                        la cosmovisión explica que somos nacidos desde los grandes derrumbes originados
                                        en dos lagunas llamadas ÑIMPI (Laguna hembra) y NU PITRAPU (laguna macho), estos
                                        están acompañados por los grandes espíritus naturales como es Kallím y Pishimisak
                                        junto con el Aro Iris que dieron luz y vida. De los derrumbes nacieron los llamados
                                        en su lenguaje PISHAU, por que vinieron en los derrumbes y fueron rescatados por
                                        Pishimisak y Kallím. Más tarde poblarían gran parte del cauca que en el lenguaje
                                        WAM significa de los bosques, la ocupación de los Pishau se daría incluyendo Popayán,
                                        en donde dice la otra historia que fue fundada por Sebastián de Belalcázar… cuando
                                        llegaron los españoles ya la ciudad existía bajo el sol… largas guerras, tremendos
                                        esfuerzos, enormes crímenes fueron necesarios para que Ampudia y Añasco venciera
                                        al Cacique Payan y le diera muerte…3 La otra versión tiene que ver con el estudio
                                        que realizó Héctor Llanos Vargas, en su texto; Los cacicazgos de Popayán a la llegada
                                        de los conquistadores (Fundación de Investigación Arqueológicas Nacionales Banco
                                        de la Republica-Bogotá 1981), cuyo trabajo tuvo como objetivo ver desde la historia
                                        el estado de las tribus indígenas de la jurisdicción de la ciudad de Popayán, a
                                        la llegada de los primeros conquistadores. Según el autor una de las provincias
                                        y pueblos de indios de Popayán fueron los PUBENENSES, quienes encontraron con las
                                        huestes españolas en el valle de Pubenza. Llanos Vargas, retoma lo de Cieza de León,
                                        y dice: “Todas estas vegas y valles fueron primero muy pobladas por el señor llamado
                                        Popayán” (1981: 17-18). También menciona lo que habla el cronista Herrera de la
                                        llegada de Belalcázar, “halló que aquella campiña tenía muchas diversas estancias
                                        desde aquel sitio hasta una braza del rio grande que son catorce leguas de muy lindas
                                        vegas cultivadas…” a la vez asocia estas tierras al Cacique Payán y a su hermano
                                        Calambás (1981: 18). Así, menciona a la provincia de WAMPIA, ubicada hacia la parte
                                        oriente de la ciudad de Popayán (esta es localizada por Cieza de León), de igual
                                        forma aparecen en los libros tributarios de Popayán elaboradas por el visitador
                                        Tomás López en el año 1559 . -->
                                         </p>
                                    
                                    <p> <iframe width="560" height="315" src="https://www.youtube.com/embed/R0VI1UMhg-M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>
                                    <ul class="list-inline"align="justify"style="font-family: sans-serif">                
                                        <li>Bibliografia:</li>
                                        <li>Tomado de: PLAN DE SALVAGUARDA PARA EL PUEBLO MISAK <<recuperar la tierra para
                                            recuperarlo todo>> (1): Avelino Dagua y Luis Guillermo Vasco. ARO IRIS. (2): Testimonio
                                            recogido por el equipo de investigadores Misak. (3): Tomado de: Dagua Hurtado Avelino,
                                            Aranda Misael y Vasco Uribe Luis Guillermo. Somos Raíz y retoño. Comité de Historia
                                            del Cabildo del pueblo Wampiano, Fundación Colombia Nuestra, Universidad Nacional
                                            de Colombia 2° Edición 1999. Pág. 1-5 (4) Tomado del anteproyecto de grado del estudiante
                                            de historia Luis Eduardo Calambás presentado al departamento e historia de la Universidad
                                            del Cauca. </li>
                                    </ul>
                                    <button class="btn btn-primary" data-dismiss="modal" type="button">
                                        <i class="fa fa-times"></i>Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal 2 -->
        <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-dismiss="modal">
                        <div class="lr">
                            <div class="rl">
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 mx-auto">
                                <div class="modal-body">
                                    <!-- Project Details Go Here -->
                                    <h2>
                                        Namui wam-Nuestro Idioma</h2>
                                    <img class="img-fluid d-block mx-auto" src="public/img/portfolio/imagen-idioma.jpg" alt="">
                                    <p class="text-muted"align="justify" style="font-family: sans-serif">
                                        NAMUI WAN – NUESTRO IDIOMA A pesar que a primera vista nuestro idioma es bastante
                                        fuerte, los datos recogidos en él diagnóstico presentan otra realidad. El 64,2 %
                                        de los Misak hablamos nuestro idioma, es decir, somos bilingües. Mientras que el
                                        35,8 % somos pasivos (entendemos el idioma pero no lo hablamos). Es decir, son personas
                                        que, aunque la mayoría entienden el idioma, ya no lo hablan. La migración de la
                                        población Misak a otros espacios territoriales, la generalización de la radio y
                                        la televisión, las mujeres que viajan a trabajar en casas de familia, los jóvenes
                                        que se van al ejército, introducen nuevas formas de hablar y comportarse, que el
                                        resto de la comunidad asimila, modificando los usos, costumbres y el idioma Namtrik
                                        ancestral. Todo esto ha puesto en crisis nuestro sistema educativo, que abandonó
                                        la experiencia de fortalecimiento del pensamiento y los idiomas propios de los muchos
                                        años. Por ello nuestro idioma está perdiendo gradualmente el uso de las palabras
                                        que relacionaban la naturaleza con el hombre. La biodiversidad, educación, con la
                                        salud y con la justicia. Fenómeno que nos pone en la obligación moral y política
                                        de generar iniciativas y propuestas educativas y culturales tendientes a salvaguardar
                                        no sólo nuestra lengua, sino a través de ella nuestra independencia política internacional
                                        que arremete contra nuestras culturas y territorios.</p>
                                        
                                            <!-- Los Misak que nos hemos visto
                                        en la obligación de desplazarnos a territorios distintos al resguardo de Wambia,
                                        hemos vivido inicialmente entre población mestiza. Nuestros hijos han estudiado
                                        en escuelas no indígenas, se han socializado con otros niños y niñas que hablan
                                        solo castellano y eso ha incidido para que ellos no hablen el NAMUI WAM. Muchas
                                        veces ni los padres de familia no lo hablamos con ellos (nuestro idioma) porque
                                        queremos que se defiendan con el castellano o que los demás niños no se burlen de
                                        él. La educación formal hasta hace pocos años era en castellano. Los niños y niñas
                                        que no hablaban el castellano tenían muchas dificultades en la escuela. Esto ha
                                        cambiado en la actualidad, con la vinculación de docentes Misak. Sin embargo, el
                                        ambiente educativo aún incide en la perdida de nuestro idioma, pues la inmensa mayoría
                                        de los textos se encuentran escritos en castellano. En relación con la lectura y
                                        escritura del NAMUI WAM, solo el 10% de la población que lo habla es capaz de leerlo
                                        y escribirlo de acuerdo con el alfabeto unificado adoptado por el NU NAKCHAK en
                                        el año 2010. No existe una metodología clara para la enseñanza de la lectoescritura
                                        de nuestro idioma, ni los materiales necesarios. -->
                                    
                                    <p> <iframe width="560" height="315" src="https://www.youtube.com/embed/umo5ssmH79E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>
                                    <ul class="list-inline">
                                        <li>Bibliografia:Manifiesto Guambiano </li>
                                    </ul>
                                    <button class="btn btn-primary" data-dismiss="modal" type="button">
                                        <i class="fa fa-times"></i>Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal 3 -->
        <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-dismiss="modal">
                        <div class="lr">
                            <div class="rl">
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 mx-auto">
                                <div class="modal-body">
                                    <!-- Project Details Go Here -->
                                    <h2>
                                        Vestuario-Namui kewakuile</h2>
                                    <img class="img-fluid d-block mx-auto" src="public/img/portfolio/03-full.jpg" alt="">
                                    <p class="text-muted"align="justify" style="font-family: sans-serif">
                                        Vestido Misak, La ruana: El color negro de la ruana y anaco, representan la madre
                                        tierra; la ruana gris simboliza la unidad de la comunidad, - un estado intermedio
                                        de equilibrio entre la tierra y la paz -. La ruana blanca representa la pureza.
                                        Una mujer experta en el tejido Guambiano, hila la medida (lana) de una ruana completa
                                        tres horas diarias, durante diez días. Hace la armada del hilo en el telar durante
                                        1 día y teje la ruana durante otros diez días. El reboso: El color azul del reboso
                                        representa las aguas, las lagunas y el cielo. Adicionalmente recuerda el origen,
                                        porque el guambiano concibe su origen en el agua. Los rebosos o mantas antiguas
                                        eran habanas y simbolizaban la paz y el carácter de los Guambianos. La bufanda y
                                        las gargantillas de la mujer: La bufanda y las gargantillas, representan la familia
                                        y por esta razón son prendas de vestir que hay que mantenerlas permanentemente puestas
                                        y no abandonarlas, como no se abandona a la familia que está siempre en el recuerdo.
                                        Antiguamente la mujer Guambiana lucio collares de semilla de mejicano, después adoptaron
                                        un collar de varias vueltas como de vidrio brillante con pepitas azules, rojas y
                                        pintadas que se conjugaban con los cruceros y luego se reemplazó por las gargantillas
                                        de perlas que se usan hoy. El peso de la gargantilla varía según el gusto de la
                                        persona, unas usaban de dos arrobas, otras de una arroba y otras de media arroba.
                                        Su cantidad era símbolo de belleza y elegancia, además se consideraba una excelente
                                        combinación con el cabello corto generalizado de las mujeres. El uso de dos arrobas
                                        era el más frecuente para fiestas y matrimonios Las mochilas se utilizaban de diferentes
                                        tamaños tanto en el traje del hombre como en el de la mujer. La mochila pequeña,
                                        hacia parte del traje de la mujer, al punto de que, si no se llevaba, se tenía la
                                        sensación de que faltaba algo. Cuando se bajaba a Silvia, se tenía por costumbre
                                        dos y tres mochilas. El sombrero (Tampalkuari): Según los abuelos el Tampalkuari
                                        la espirar que recoge la base filosófica del pensamiento Guambiano. Recuerda al
                                        Guambiano guardar las ideas y para guardar el pensamiento. María Antonia Tumiña
                                        dijo “mi padre decía que ahí estaba el pensamiento, que desde que empieza hasta
                                        que termine mantiene unido, y que, a pesar de sus pintas y diseños, permanece en
                                        el camino que es continuo, es la espiral, es el pensamiento. Los abuelos decían
                                        que lo usaban cuando llovía para que la lluvia no les fuera a quitar los pensamientos.
                                        El sombrero era una especie de protección para que el agua pasara derecho y no pudiera
                                        quitar los pensamientos”.
                                    </p>
                                    <p> <iframe width="560" height="315" src="https://www.youtube.com/embed/JHVarXEANSo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
                                    <ul class="list-inline">
                                        <li>Bibliografia:Manifiesto Guambiano</li>
                                    </ul>
                                    <button class="btn btn-primary" data-dismiss="modal" type="button">
                                        <i class="fa fa-times"></i>Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal 4 -->
        <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-dismiss="modal">
                        <div class="lr">
                            <div class="rl">
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 mx-auto">
                                <div class="modal-body">
                                    <!-- Project Details Go Here -->
                                    <h2>Gastronomia</h2>
                                    <img class="img-fluid d-block mx-auto" src="public/img/portfolio/imagen-gastronomia.jpg" alt="">
                                    <p class="text-muted"align="justify" style="font-family: sans-serif">
                                        Silvia, municipio rico en cultura, tradición y ornamentación indígena. Sus verdes
                                        e inminentes paisajes más el diseño de sus casas, convierten este lugar en “La suiza
                                        de América”. Este es sencillamente un lugar de gente cálida, alegre y resistente
                                        a abandonar costumbres. Uno de los principales atractivos de Silvia llega cada martes,
                                        cuando las comunidades indígenas se citan en el parque principal para intercambiar
                                        sus mercancías sin la intervención del dinero. El trueque sigue siendo el mecanismo
                                        de intercambio comercial utilizado por los indígenas, que llama la atención de decenas
                                        de turistas que observan con sorpresa la nutrida plaza de mercado en que se trasforma
                                        ‘La Galería’, como se le llama a la plaza central. Los guámbianos consideran a la
                                        naturaleza como madre y espíritu de vida. Cultivan maíz, papa y cebolla. Se valen
                                        de las mingas, que son sistemas de cooperación entre la comunidad, para los trabajos
                                        en el campo, para la construcción de estructuras y para actividades que traigan
                                        el beneficio colectivo. Además, llevan a cabo un amplio proyecto de producción de
                                        trucha que venden a otros departamentos del país, y actualmente están gestionando
                                        la exportación de su producto a otros países.
                                    </p>
                                    <p> <iframe width="560" height="315" src="https://www.youtube.com/embed/3Azf-DxkNMY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
                                    <ul class="list-inline">
                                        <li>Bibliografia:http://www.experienciacolombia.com/destino.php?Region-Occidental=Silvia(Cauca)&Silvia&destino=41</li>
                                    </ul>
                                    <button class="btn btn-primary" data-dismiss="modal" type="button">
                                        <i class="fa fa-times"></i>Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal 5 -->
        <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-dismiss="modal">
                        <div class="lr">
                            <div class="rl">
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 mx-auto">
                                <div class="modal-body">
                                    <!-- Project Details Go Here -->
                                    <h2>
                                        Mitos y Leyendas</h2>
                                    <img class="img-fluid d-block mx-auto" src="public/img/portfolio/imagen-mitos.jpg" alt="">
                                    <p class="text-muted"align="justify" style="font-family: sans-serif">
                                        El siguiente mito guambiano de la creación fue tomado de la primera parte de un
                                        relato aparecido en la página web del antropólogo colombiano Luis Guillermo Vasco.
                                        Cuenta la siguiente historia: . Primero era la tierra... y eran las lagunas, grandes
                                        lagunas. La mayor de todas era la de Nupisu, Piendamó, en el centro de la sabana,
                                        del páramo, como una matriz, como un corazón; es Nupirrapu, que es un hueco muy
                                        profundo. El agua es vida. Primero eran la tierra y el agua. El agua no es buena
                                        ni es mala. De ella resultan cosas buenas y cosas malas. Allá, en las alturas, era
                                        el agua. Llovía intensamente, con aguaceros, borrascas, tempestades. Los ríos venían
                                        grandes, con inmensos derrumbes que arrastraban las montañas y traían piedras como
                                        casas; venían grandes crecientes e inundaciones. Era el agua mala. En ese tiempo,
                                        estas profundas guaicadas (hondonadas entre dos montañas) y estas peñas no eran
                                        así, como las vemos hoy, todo esto era pura montaña, esos ríos las hicieron cuando
                                        corrieron hasta formar el mar. El agua es vida. Nace en las cabeceras y baja en
                                        los ríos hasta el mar. Y se devuelve, pero no por los mismos ríos sino por el aire,
                                        por la nube. Subiendo por las guaicadas y por los filos de las montañas alcanza
                                        hasta el páramo, hasta las sabanas, y cae otra vez la lluvia, cae el agua que es
                                        buena y es mala. Allá arriba, como la tierra y el agua, estaba él-ella (El Pishimisak
                                        es la unidad perfecta, el par perfecto; encierra en su ser los dos principios, lo
                                        masculino y lo femenino, que juntos dan la multiplicación; pero, a la vez, se conforma
                                        en dos personajes: el Pishimisak propiamente dicho y el Kallim). Es el Pishimisak,
                                        a la vez masculino y femenino, que también ha existido desde siempre, todo blanco,
                                        todo bueno, todo fresco. Del agua nació el kosrompoto, el aroiris que iluminaba
                                        todo con su luz; allí brillaba, el Pishimisak lo veía alumbrar. Dieron mucho fruto,
                                        dieron mucha vida. El agua estaba arriba, en el páramo. Abajo se secaban las plantas,
                                        se caían las flores, morían los animales. Cuando bajó el agua, todo creció y floreció,
                                        retoñó toda la hierba y hubo alimentos aquí. Era el agua buena. Antes, en las sabanas
                                        del páramo, el Pishimisak tenía todas las comidas, todos los alimentos. El-ella
                                        es el dueño de todo. Ya estaba allí cuando se produjeron los derrumbes que arrastrando
                                        gigantescas piedras formaron las guaicadas. Pero hubo otros derrumbes. A veces el
                                        agua no nacía en las lagunas para correr hacia el mar, sino que se filtraba en la
                                        tierra, la removía, la aflojaba y entonces caían los derrumbes. Estos se desprendieron
                                        desde muchos siglos adelante, dejando grandes heridas en las montañas. De ellos
                                        salieron los humanos que eran la raíz de los nativos. Al derrumbe le decían pirran
                                        uno, es decir, parir el agua. A los humanos que allí nacieron los nombraron los
                                        Pishau. Los Pishau vinieron en los derrumbes, llegaron en las crecientes de los
                                        ríos. Por debajo del agua venían arrastrándose y golpeando las grandes piedras,
                                        encima de ellas venía el barro, la tierra, luego el agua sucia; en la superficie
                                        venía la palizada, las ramas, las hojas, los árboles arrancados y, encima de todo,
                                        venían los niños, chumbados. Los anteriores nacieron del agua, venidos en los shau,
                                        restos de vegetación que arrastra la creciente. Son nativos de aquí de siglos y
                                        siglos. En donde salía el derrumbe, en la gran herida de la tierra, quedaba olor
                                        a sangre; es la sangre regada por la naturaleza, así como una mujer riega la sangre
                                        al dar a luz a un niño. Los Pishau no eran otras gentes, eran los mismos guambianos,
                                        gigantes muy sabios que comían sal de aquí, de nuestros propios salados, y no eran
                                        bautizados. Ellos ocuparon todo nuestro territorio, ellos construyeron todo nuestro
                                        Nupirau antes de llegar los españoles. Era grande nuestra tierra y muy rica. En
                                        ella teníamos minas de minerales muy valiosos, como el oro que se encontraba en
                                        Chisquío, en San José y en Corrales, también maderas finas, peces, animales del
                                        monte y muchos otros recursos que sabíamos utilizar con nuestro trabajo para vivir
                                        bien.
                                    </p>
                                    <p> <iframe width="560" height="315" src="https://www.youtube.com/embed/19fUXawv5c8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>
                                    <ul class="list-inline">
                                        <li>Bibliografia:http://mitosla.blogspot.com.co/2008/03/colombia-mito-guambiano-creacin.html</li>
                                    </ul>
                                    <button class="btn btn-primary" data-dismiss="modal" type="button">
                                        <i class="fa fa-times"></i>Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal 6 -->
        <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-dismiss="modal">
                        <div class="lr">
                            <div class="rl">
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 mx-auto">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript -->

        <script src="public/vendor/jquery/jquery.min.js"></script>

        <script src="public/jquery-ui.js"></script>

        <script src="public/vendor/popper/popper.min.js"></script>

        <script src="public/vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->

        <script src="public/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Contact form JavaScript -->

        <script src="public/js/jqBootstrapValidation.js"></script>

        <script src="public/js/contact_me.js"></script>

        <!-- Custom scripts for this template -->

        <script src="public/js/agency.min.js"></script>

        <script type="text/javascript">
        $( document ).ready(function() {
        $('#txtPalabra').on('keydown', function(e) {
    if (e.which == 13) {
        e.preventDefault();
    }
});
        $( "#txtPalabra" ).autocomplete({
              source: function( request, response ) {

                  var dataValue = "{ palabra: '" + $("#txtPalabra").val() + "' }";

                    $.  ajax({
                        type: "POST",
                        url: "Default.aspx/OnSubmit",
                        data: dataValue,                
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
                        },
                        success: function (result) {
                            var list = JSON.parse(result.d)
                            
                            response( list );
                        }
                    });

              },
              minLength: 2,
              select: function( event, ui ) {
                var linea = ui.item.value;
               var linea = linea.split("|")
                var espanol =linea[0];
                var misak =linea[1];
                var ingles=linea[2];
                var imagen=linea[3];
                var descripcion=linea[4];
                
                
                $("#lbespañol").text(espanol);
                $("#lbmisak").text(misak);
                $("#lbingles").text(ingles);
                $("#lbdescripcion").text(descripcion);
                $("#Imagen").attr("src", imagen)
                
                ui.item.value = espanol;
              }
            } ).autocomplete( "instance" )._renderItem = function( ul, item ) {
                var palabra = $("#txtPalabra").val()
                var linea = item.value;             
                var linea = linea.split("|")
                var espanol = linea[0].replace(palabra, "<b>"+palabra+"</b>");
                var misak =linea[1].replace(palabra, "<b>"+palabra+"</b>");;
                var ingles=linea[2].replace(palabra, "<b>"+palabra+"</b>");
              return $( "<li>" )
                .append( "<div>" + espanol + "->" + misak + "->" + ingles +"</div>" )
                .appendTo( ul );
            };
          });
        

        </script>

    </body>
    </form>
</body>
</html>
