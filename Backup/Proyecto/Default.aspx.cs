﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Proyecto
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string OnSubmit(string palabra)
        {
            List<String> result = new List<String>(); 

            //Leer archivo, recorrerlo y buscar cada fila que contenga: palabra
            //result = result + fila;

            string[] lineas = System.IO.File.ReadAllLines(HttpContext.Current.Server.MapPath("~/public/Diccionario.txt"));

            bool palabraExiste = false;

            foreach (string line in lineas)
            {
                string[] arrCampos = line.Split('|');
                // crear todas las variables del dicccionario
                string IdiomaEspanol = arrCampos[0];
                string IdiomaMisak = arrCampos[1];
                string IdiomaIngles = arrCampos[2];
                string DescripcionPalabra = arrCampos[3];
                string Imagen = arrCampos[4];


                if (IdiomaEspanol.ToLower().Contains(palabra.ToLower()) || IdiomaIngles.ToLower().Contains(palabra.ToLower())|| IdiomaMisak.ToLower().Contains(palabra.ToLower()))
                {
                    result.Add(line);
                }

            }
            var json = JsonConvert.SerializeObject(result);
            return json;

        }
    }
}
